﻿using UnityEditor;

namespace com.fdt.genericdatasaving.editor
{
    [CustomEditor(typeof(GenericDataSave), true)]
    public class GenericDataSaveEditor : DataSaveEditorBase
    {
        
    }
}