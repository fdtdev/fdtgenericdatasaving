﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using com.fdt.genericdatasaving;
using UnityEditor;
using UnityEngine;

namespace com.fdt.genericdatasaving.editor
{
    [CustomPropertyDrawer(typeof(ValueData))]
    public class ValueDataEditor : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            SerializedProperty _typeProp = property.FindPropertyRelative("_varType");
            VarType t = (VarType) _typeProp.intValue;
            string vprop = string.Empty;

            switch (t)
            {
                case VarType.INT:
                    vprop = "_intValue";
                    break;
                case VarType.BOOL:
                    vprop = "_boolValue";
                    break;
                case VarType.FLOAT:
                    vprop = "_floatValue";
                    break;
                case VarType.STRING:
                    vprop = "_stringValue";
                    break;
                case VarType.UOBJECT:
                    vprop = "_uObjectValue";
                    break;
                case VarType.ASSET:
                    vprop = "_assetValue";
                    break;
                case VarType.VECTOR3:
                    vprop = "_vector3Value";
                    break;
            }

            //EditorGUI.PropertyField(new Rect(position.x, position.y, position.width, 16),  _typeProp);

            if (t != VarType.NOT_SET)
            {
                SerializedProperty sp = property.FindPropertyRelative(vprop);
                float h = EditorGUI.GetPropertyHeight(sp, true);
                if (!string.IsNullOrEmpty(label.text))
                {
                    EditorGUI.PropertyField(new Rect(position.x, position.y /*+18*/, position.width, h), sp, label, true);    
                }
                else
                {
                    EditorGUI.PropertyField(new Rect(position.x, position.y /*+18*/, position.width, h), sp, true);
                }
                
            }

            EditorGUI.EndProperty();
        }

        public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
        {
            SerializedProperty _typeProp = property.FindPropertyRelative("_varType");
            VarType t = (VarType) _typeProp.intValue;
            string vprop = string.Empty;
            /*if (t == FightVarType.NOT_SET)
                return 18;
            else*/
            {
                switch (t)
                {
                    case VarType.INT:
                        vprop = "_intValue";
                        break;
                    case VarType.BOOL:
                        vprop = "_boolValue";
                        break;
                    case VarType.FLOAT:
                        vprop = "_floatValue";
                        break;
                    case VarType.STRING:
                        vprop = "_stringValue";
                        break;
                    case VarType.UOBJECT:
                        vprop = "_uObjectValue";
                        break;
                    case VarType.ASSET:
                        vprop = "_assetValue";
                        break;
                    case VarType.VECTOR3:
                        vprop = "_vector3Value";
                        break;
                }

                SerializedProperty sp = property.FindPropertyRelative(vprop);
                float h = 18;
                if (sp != null)
                    h = EditorGUI.GetPropertyHeight(sp, true);
                //return 18+h;
                return h;
            }
        }
    }
}