﻿using System.Collections;
using System.Collections.Generic;
using com.fdt.genericdatasaving;
using com.fdt.statemachinegraph;
using UnityEditor;
using UnityEngine;

namespace com.fdt.statemachinegraph.editor
{
    [CustomPropertyDrawer(typeof(IDataSaveProperty))]
    public class IDataSavePropertyDrawer : PropertyDrawer
    {
        public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
        {
            EditorGUI.BeginProperty(position, label, property);
            EditorGUI.BeginChangeCheck();
            UnityEngine.Object o = property.objectReferenceValue; 
            o = EditorGUI.ObjectField(position, label, o, typeof(IDataSave), true);
            if (EditorGUI.EndChangeCheck())
            {
                if (o is IDataSave)
                {
                    property.objectReferenceValue = o;
                    property.serializedObject.ApplyModifiedProperties();
                }
            }

            EditorGUI.EndProperty();

        }
    }
}