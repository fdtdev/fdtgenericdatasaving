﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;
using UnityEditor;
using Object = UnityEngine.Object;

namespace com.fdt.genericdatasaving.editor
{
    public class DataSaveEditorBase : UnityEditor.Editor
    {
        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            DrawDefaultInspector();

            var genVarList = (target as IDataSave).genVarList;

            foreach (var v in genVarList)
            {
                object o = (target as IDataSave).GetValue(v);
                DrawVar(v, o);
                
            }

            serializedObject.ApplyModifiedProperties();
        }

        private void DrawVar(string v, object d)
        {
            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField(v);
            if (d is UnityEngine.Object)
            {
                EditorGUILayout.ObjectField((Object) d, typeof(UnityEngine.Object), false);
            }
            else if (d is bool)
            {
                bool boolvalue = (bool) d;
                EditorGUILayout.LabelField(boolvalue ? "True" : "False");
            }
            else if (d!= null)
            {
                MemberInfo method = typeof(String).GetMethod(
                    "ToString",
                    BindingFlags.Instance | BindingFlags.Public | BindingFlags.DeclaredOnly,
                    null,
                    new Type[] { },// Method ToString() without parameters
                    null);
                if (method != null)
                {
                    EditorGUILayout.LabelField(d.ToString());    
                }
            }
             
            EditorGUILayout.EndHorizontal();
        }
    }
}