﻿using UnityEditor;

namespace com.fdt.genericdatasaving.editor
{
    [CustomEditor(typeof(GlobalDataSave), true)]
    public class GlobalDataSaveEditor : DataSaveEditorBase
    {
        
    }
}