# FDT Generic Data Saving

Generic scriptableobject vars with custom serialized data.


## Usage

The package must be included using the manifest.json file in the project.
The file must be modified to include this dependencies:

```json
{
  "dependencies": {
	"com.fdt.genericdatasaving": "https://bitbucket.org/fdtdev/fdtgenericdatasaving.git#4.0.0",
	"com.fdt.common": "https://bitbucket.org/fdtdev/fdtcommon.git#5.0.0",

	...
  }
}

```

## License

MIT - see [LICENSE](https://bitbucket.org/fdtdev/fdtgenericdatasaving/src/4.0.0/LICENSE.md)