﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using com.FDT.Common;
using com.fdt.statemachinegraph;
using UnityEngine;
using Object = UnityEngine.Object;

namespace com.fdt.genericdatasaving
{
    public class GenericDataSave : MonoBehaviour, IDataSave
    {
        [SerializeField] protected DataSave data;
        public IEnumerable<string> genVarList
        {
            get { return data.genVarList; }
        }

        public void ResetValue(string var)
        {
            data.ResetValue(var);
        }
        public void AddValue(string var, int v)
        {
            data.ValidateData(var, 0);
            data.AddValue(var, v);
        }
        public int GetInt(string var)
        {
            data.ValidateData(var, 0);
            return data.GetInt(var);
        }
        public void AddValue(string var, float v)
        {
            data.ValidateData(var, 0f);
            data.AddValue(var, v);
        }
        public float GetFloat(string var)
        {
            data.ValidateData(var, 0f);
            return data.GetFloat(var);
        }
        public void AddValue(string var, string v)
        {
            data.ValidateData(var, string.Empty);
            data.AddValue(var, v);
        }
        public string GetString(string var)
        {
            data.ValidateData(var, string.Empty);
            return data.GetString(var);
        }
        public bool GetBool(string var)
        {
            data.ValidateData(var, false);
            return data.GetBool(var);
        }
        public void AddValue(string var, Vector3 v)
        {
            data.ValidateData(var, Vector3.zero);
            data.AddValue(var, v);
        }
        public Vector3 GetVector3(string var)
        {
            data.ValidateData(var, Vector3.zero);
            return data.GetVector3(var);
        }
        public void SetValue(string var, Object v)
        {
            data.ValidateData(var, null);
            data.SetValue(var, v);
        }
        public Object GetUObject(string var)
        {
            data.ValidateData(var, null);
            return data.GetUObject(var);
        }
        public void SetValue(string var, object valueData)
        {
            data.ValidateData(var, null);
            data.SetValue(var, valueData);
        }
        public object GetValue(string var)
        {
            data.ValidateData(var, null);
            return data.GetValue(var);
        }
        public bool DataEquals(string var, object valueData)
        {
            return data.DataEquals(var, valueData);
        }
        public bool HasValue(string var)
        {
            return data.HasValue(var);
        }
        
         public void ResetValue(IDAsset var)
        {
            ResetValue(var.name);
        }

        public void AddValue(IDAsset var, int v)
        {
            AddValue(var.name, v);
        }

        public int GetInt(IDAsset var)
        {
            return GetInt(var.name);
        }

        public void AddValue(IDAsset var, float v)
        {
            AddValue(var.name, v);
        }

        public float GetFloat(IDAsset var)
        {
            return GetFloat(var.name);
        }

        public void AddValue(IDAsset var, string v)
        {
            AddValue(var.name, v);
        }

        public string GetString(IDAsset var)
        {
            return GetString(var.name);
        }

        public bool GetBool(IDAsset var)
        {
            return GetBool(var.name);
        }

        public void AddValue(IDAsset var, Vector3 v)
        {
            AddValue(var.name, v);
        }

        public Vector3 GetVector3(IDAsset var)
        {
            return GetVector3(var.name);
        }

        public void SetValue(IDAsset var, Object v)
        {
            SetValue(var.name, v);
        }

        public Object GetUObject(IDAsset var)
        {
            return GetUObject(var.name);
        }

        public void SetValue(IDAsset var, object valueData)
        {
            SetValue(var.name, valueData);
        }

        public object GetValue(IDAsset var)
        {
            return GetValue(var.name);
        }

        public bool DataEquals(IDAsset var, object valueData)
        {
            return DataEquals(var.name, valueData);
        }

        public bool HasValue(IDAsset var)
        {
            return HasValue(var.name);
        }
    }
}