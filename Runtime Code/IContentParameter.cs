﻿namespace com.fdt.genericdatasaving
{
    public interface IContentParameter
    {
        System.Type GetAssetType { get; }
        bool GetTypeFromAsset { get; }
        
        string GetCustomDescription { get; }
        VarType GetVarType { get; }
    }
}