﻿using System;
using UnityEngine;

namespace com.fdt.statemachinegraph
{
    [AttributeUsage(AttributeTargets.Field, Inherited = true)]
    public class IDataSaveProperty : PropertyAttribute
    {
        public IDataSaveProperty ()
        {

        }
    }
}