﻿using System.Collections;
using System.Collections.Generic;
using com.FDT.Common;
using UnityEngine;

namespace com.fdt.genericdatasaving
{
    public interface IGenericDataSave
    {
        IEnumerable<string> genVarList { get; }
        void ResetValue(string @var);
        void AddValue(string @var, int v);
        int GetInt(string @var);
        void AddValue(string @var, float v);
        float GetFloat(string @var);
        void AddValue(string @var, string v);
        string GetString(string @var);
        bool GetBool(string @var);
        void AddValue(string @var, Vector3 v);
        Vector3 GetVector3(string @var);
        void SetValue(string @var, UnityEngine.Object v);
        UnityEngine.Object GetUObject(string @var);
        void SetValue(string @var, object valueData);
        object GetValue(string @var);
        bool DataEquals(string @var, object valueData);
        bool HasValue(string @var);
        
        void ResetValue(IDAsset @var);
        void AddValue(IDAsset @var, int v);
        int GetInt(IDAsset @var);
        void AddValue(IDAsset @var, float v);
        float GetFloat(IDAsset @var);
        void AddValue(IDAsset @var, string v);
        string GetString(IDAsset @var);
        bool GetBool(IDAsset @var);
        void AddValue(IDAsset @var, Vector3 v);
        Vector3 GetVector3(IDAsset @var);
        void SetValue(IDAsset @var, UnityEngine.Object v);
        UnityEngine.Object GetUObject(IDAsset @var);
        void SetValue(IDAsset @var, object valueData);
        object GetValue(IDAsset @var);
        bool DataEquals(IDAsset @var, object valueData);
        bool HasValue(IDAsset @var);
    }
}