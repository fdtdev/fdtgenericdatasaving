﻿using System;
using UnityEngine;

namespace com.fdt.genericdatasaving
{
    [System.Serializable]
    public struct ValueData:IEquatable<ValueData>
    {
        [SerializeField] private VarType _varType;
        [SerializeField] private bool _boolValue;
        [SerializeField] private int _intValue;
        [SerializeField] private float _floatValue;
        [SerializeField] private string _stringValue;
        [SerializeField] private ScriptableObject _assetValue;
        [SerializeField] private UnityEngine.Object _uObjectValue;
        [SerializeField] private Vector3 _vector3Value;
        

        public bool BoolValue
        {
            get { return _boolValue; }
            set { _boolValue = value; }
        }

        public int IntValue
        {
            get { return _intValue; }
            set { _intValue = value; }
        }

        public float FloatValue
        {
            get { return _floatValue; }
            set { _floatValue = value; }
        }

        public string StringValue
        {
            get { return _stringValue; }
            set { _stringValue = value; }
        }

        public UnityEngine.Object UObjectValue
        {
            get { return _uObjectValue; }
            set { _uObjectValue = value; }
        }
        
        public ScriptableObject AssetValue
        {
            get { return _assetValue; }
            set { _assetValue = value; }
        }
        
        public VarType VarType
        {
            get { return _varType; }
            set { _varType = value; }
        }
        
        public Vector3 Vector3Value
        {
            get { return _vector3Value; }
            set { _vector3Value = value; }
        }

        public ValueData Clone()
        {
            ValueData result = new ValueData();
            result._varType = _varType;
            result._boolValue = _boolValue;
            result._intValue = _intValue;
            result._stringValue = _stringValue;
            result._floatValue = _floatValue;
            result._uObjectValue = _uObjectValue;
            result._assetValue = _assetValue;
            result._vector3Value = _vector3Value;
            return result;

        }

        public void SetData(ValueData valueData)
        {
            // only sets the data of the right type
            switch (_varType)
            {
                case VarType.INT:
                    _intValue = valueData._intValue;
                    break;
                case VarType.BOOL:
                    _boolValue = valueData._boolValue;
                    break;
                case VarType.STRING:
                    _stringValue = valueData._stringValue;
                    break;
                case VarType.FLOAT:
                    _floatValue = valueData._floatValue;
                    break;
                case VarType.UOBJECT:
                    _uObjectValue = valueData._uObjectValue;
                    break;
                case VarType.ASSET:
                    _assetValue = valueData._assetValue;
                    break;
                case VarType.VECTOR3:
                    _vector3Value = valueData._vector3Value;
                    break;
            }
        }

        public bool EqualsData(ValueData valueData)
        {
            if (_varType != valueData._varType)
                return false;

            switch (_varType)
            {
                case VarType.INT:
                    return _intValue == valueData._intValue;
                case VarType.BOOL:
                    return _boolValue == valueData._boolValue;
                case VarType.STRING:
                    return _stringValue == valueData._stringValue;
                case VarType.FLOAT:
                    return Mathf.Approximately(_floatValue, valueData._floatValue);
                case VarType.UOBJECT:
                    return _uObjectValue == valueData._uObjectValue;
                case VarType.ASSET:
                    return false;
                case VarType.VECTOR3:
                    return _vector3Value == valueData._vector3Value;
            }

            return false;
        }

        public static bool operator == (ValueData a, ValueData b)
        {
            if(object.ReferenceEquals(a, b))
                return true;
 
            // if both a and b were null, we would have already escaped so check if either is null
            if(object.ReferenceEquals(a,null))
                return false;
            if(object.ReferenceEquals(b,null))
                return false;
            
            if (a._varType != b._varType)
                return false;
            
            switch (a._varType)
            {
                case VarType.INT:
                    return a._intValue == b._intValue;
                case VarType.BOOL:
                    return a._boolValue == b._boolValue;
                case VarType.FLOAT:
                    return Mathf.Approximately(a._floatValue, b._floatValue);
                case VarType.STRING:
                    return a._stringValue == b._stringValue;
                case VarType.ASSET:
                    return a._assetValue == b._assetValue;
                case VarType.VECTOR3:
                    return a._vector3Value == b._vector3Value;
                case VarType.UOBJECT:
                    return a._uObjectValue == b._uObjectValue;
            }
            return false;
        }

        public static bool operator !=(ValueData a, ValueData b)
        {
            return !(a == b);
        }
        
        public bool Equals(ValueData other)
        {
            if (ReferenceEquals(null, other)) return false;
            if (ReferenceEquals(this, other)) return true;
            return _varType == other._varType && _boolValue == other._boolValue && _intValue == other._intValue &&
                   _floatValue.Equals(other._floatValue) && string.Equals(_stringValue, other._stringValue) &&
                   _uObjectValue.Equals(other._uObjectValue) && Equals(_assetValue, other._assetValue) && _vector3Value.Equals(other._vector3Value);
        }

        public override bool Equals(object obj)
        {
            if (ReferenceEquals(null, obj)) return false;
            if (ReferenceEquals(this, obj)) return true;
            if (obj.GetType() != this.GetType()) return false;
            return Equals((ValueData) obj);
        }

        public override int GetHashCode()
        {
            unchecked
            {
                var hashCode = (int) _varType;
                hashCode = (hashCode * 397) ^ _boolValue.GetHashCode();
                hashCode = (hashCode * 397) ^ _intValue;
                hashCode = (hashCode * 397) ^ _floatValue.GetHashCode();
                hashCode = (hashCode * 397) ^ (_stringValue != null ? _stringValue.GetHashCode() : 0);
                hashCode = (hashCode * 397) ^ _uObjectValue.GetHashCode();
                hashCode = (hashCode * 397) ^ _vector3Value.GetHashCode();
                hashCode = (hashCode * 397) ^ (_assetValue != null ? _assetValue.GetHashCode() : 0);
                return hashCode;
            }
        }

        public static ValueData operator +(ValueData a, ValueData b)
        {
            ValueData result = new ValueData();
            
            if(object.ReferenceEquals(a, b))
                throw new Exception("null value");
 
            // if both a and b were null, we would have already escaped so check if either is null
            if(object.ReferenceEquals(a,null))
                throw new Exception("null value");
            if(object.ReferenceEquals(b,null))
                throw new Exception("null value");
            
            if (a._varType != b._varType)
                throw new Exception("wrong var type");
            
            switch (a._varType)
            {
                case VarType.INT:
                    result._intValue = a._intValue + b._intValue;
                    break;
                case VarType.BOOL:
                    throw new Exception("no sum operator for booleans");
                case VarType.FLOAT:
                    result._floatValue = a._floatValue + b._floatValue;
                    break;
                case VarType.STRING:
                    result._stringValue = a._stringValue + b._stringValue;
                    break;
                case VarType.ASSET:
                    throw new Exception("no sum operator for assets");
            }
            result._varType = a._varType;
            return result;
        }

        public static ValueData Create(VarType v)
        {
            ValueData result = new ValueData();
            result._varType = v;
            return result;
        }
    }
}