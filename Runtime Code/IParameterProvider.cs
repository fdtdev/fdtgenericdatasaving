﻿using UnityEngine;

namespace com.fdt.genericdatasaving
{
    public interface IParameterProvider
    {
        IContentParameter[] Parameters { get; }

        Color32 BackColor { get; }
        bool TimedRange { get; }
    }
}