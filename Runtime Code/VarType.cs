﻿
namespace com.fdt.genericdatasaving
{
    public enum VarType
    {
        NOT_SET = 0,
        BOOL = 1,
        INT = 2,
        FLOAT = 3,
        STRING = 4,
        UOBJECT = 5,
        ASSET = 6,
        VECTOR3 = 7
    }
}