﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace com.fdt.genericdatasaving
{
    [System.Serializable]
    public class DataSave
    {
        public Dictionary<string, object> datas = new Dictionary<string, object>();

        protected HashSet<string> _genVarList = new HashSet<string>();

        public HashSet<string> genVarList
        {
            get { return _genVarList; }
        }
        public void AddValue(string @var, int v)
        {
            ValidateData(@var, v);
            datas[@var] = (int)datas[@var] + v;
        }

        public int GetInt(string @var)
        {
            ValidateData(@var, 0);
            return (int)datas[@var];
        }
        public void AddValue(string @var, float v)
        {
            ValidateData(@var, v);
            datas[@var] = (float)datas[@var] + v;
        }

        public float GetFloat(string @var)
        {
            ValidateData(@var, 0);
            return (float)datas[@var];
        }
        public void AddValue(string @var, string v)
        {
            ValidateData(@var, v);
            datas[@var] = (string)datas[@var] + v;
        }

        public string GetString(string @var)
        {
            ValidateData(@var, string.Empty);
            return (string)datas[@var];
        }
        public bool GetBool(string @var)
        {
            ValidateData(@var, false);
            return (bool)datas[@var];
        }
        public void AddValue(string @var, Vector3 v)
        {
            ValidateData(@var, v);
            datas[@var] = (Vector3)datas[@var] + v;
        }

        public Vector3 GetVector3(string @var)
        {
            ValidateData(@var, Vector3.zero);
            return (Vector3)datas[@var];
        }

        public void SetValue(string @var, UnityEngine.Object v)
        {
            ValidateData(@var, v);
            datas[@var] = v;
        }

        public UnityEngine.Object GetUObject(string @var)
        {
            ValidateData(@var, null);
            return (UnityEngine.Object)datas[@var];
        }
        public void ValidateData(string @var, object valueData)
        {
            if (!HasValue(@var))
            {
                datas.Add(@var, valueData);
                _genVarList.Add(@var);
            }
        }
        public void SetValue(string @var, object valueData)
        {
            if (!HasValue(@var))
            {
                datas.Add(@var, valueData);
                _genVarList.Add(@var);
            }
            else
            {
                datas[@var] = valueData;
            }
        }
        public object GetValue(string @var)
        {
            if (!HasValue(@var))
            {
                return null;
            }
            else
            {
                return datas[@var];
            }
        }
        public bool DataEquals(string @var, object valueData)
        {
            if (!HasValue(@var))
                return false;
            bool r = (datas[@var] ==  valueData);
            return r;
        }
        
        public void ResetValue(string @var)
        {
            datas[@var] = null;
        }
        public bool HasValue(string @var)
        {
            return datas.ContainsKey(@var);
        }
    }
}
